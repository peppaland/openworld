using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;

public class JumpSystem : SystemBase
{
    BuildPhysicsWorld _physicsWorld;
    EndSimulationEntityCommandBufferSystem entityCommandBufferSystem;

    protected override void OnCreate()
    {
        _physicsWorld = World.GetExistingSystem<BuildPhysicsWorld>();
        entityCommandBufferSystem = World.GetExistingSystem<EndSimulationEntityCommandBufferSystem>();

    }

    protected override void OnUpdate()
    {
        int jump = Animator.StringToHash("Jump");
        float deltaTime = Time.DeltaTime;

        EntityCommandBuffer commandBuffer = entityCommandBufferSystem.CreateCommandBuffer();


        var physicsWorld = _physicsWorld.PhysicsWorld;
        var physicsDependency = JobHandle.CombineDependencies(_physicsWorld.GetOutputDependency(), Dependency);

        //check if player on ground
        Dependency = Entities
            .WithAll<PlayerTag>()
            .ForEach((Entity entity, in Translation translation) => {

                commandBuffer.AddComponent<IsGrounded>(entity);

                //Raycast beneath player for collision with ground
                RaycastInput rayInput = new RaycastInput
                {
                    Start = translation.Value,
                    End = translation.Value.y + .01f,
                    Filter = new CollisionFilter()
                    {
                        GroupIndex = 0,
                        BelongsTo = 1u << 1,
                        CollidesWith = 1u << 2
                    }
                };


                //if raycast not colliding with ground, remove 'isGrounded'
                if (!physicsWorld.CastRay(rayInput, out Unity.Physics.RaycastHit hit))
                {
                    commandBuffer.RemoveComponent<IsGrounded>(entity);
                }

                //if raycast collides with ground, attach 'isGrounded' tag



            }).Schedule(physicsDependency);


        //Jump
        Entities.
            WithAll<IsGrounded>()
            .ForEach((Entity entity, ref PhysicsVelocity physicsVelocity, in RawInputData input, in PhysicsData physicsData) => {

                physicsVelocity.Linear.y -= 9.8f * deltaTime;

                if (input.jump)
                {
                    physicsVelocity.Linear.y = physicsData.jumpVelocity;
                }

            }).Run();

                entityCommandBufferSystem.AddJobHandleForProducer(Dependency);

        //Animations run without burst
        Entities.
        WithoutBurst().
        ForEach((Animator animator, in RawInputData input) => {
            if (input.jump)
            {
                animator.SetTrigger(jump);
            }
        }).Run();

        entityCommandBufferSystem.AddJobHandleForProducer(Dependency);

        /*
        Entities.
            WithoutBurst()
            .WithAll<PlayerTag>()
            .ForEach((in Translation translation) =>
            {
                RaycastInput rayInput = new RaycastInput
                {
                    Start = translation.Value,
                    End = translation.Value.y - 1f,
                    Filter = new CollisionFilter()
                    {
                        GroupIndex = 0,
                        BelongsTo = 1u << 1,
                        CollidesWith = 1u << 2
                    }
                };

                if (!physicsWorld.CastRay(rayInput, out Unity.Physics.RaycastHit hit)) { return; }
                Debug.DrawRay(rayInput.Start, rayInput.End, Color.red);
            }).Run();*/


    }
}

using System;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.InputSystem;
using static UnityEngine.InputSystem.InputAction;

public class ProcessInputData : SystemBase
{

    private InputActionMap inputActionMap;
    private InputAction moveAction;
    private InputAction jumpAction;
    private Vector2 moveData;
    private bool jumpData;

    //set up references to action map and move action
    public void InitializePlayerInput(PlayerInput playerInput)
    {
        inputActionMap = playerInput.actions.FindActionMap(nameof(InputControls.PlayerActionMap));
        moveAction = inputActionMap[nameof(InputControls.PlayerActionMap.Move)];
        jumpAction = inputActionMap[nameof(InputControls.PlayerActionMap.Jump)];
        SubscribeToInputActionEvent(moveAction, OnMove);
        SubscribeToInputActionEvent(jumpAction, OnJump);

    }

    //get input for movement
    public void OnMove(CallbackContext context)
    {
        moveData = context.ReadValue<Vector2>();
    }

    public void OnJump(CallbackContext context)
    {
        jumpData = context.ReadValueAsButton();
    }


    public static void SubscribeToInputActionEvent(InputAction inputAction, Action<CallbackContext> inputActionEvent)
    {
        inputAction.started += inputActionEvent;
        inputAction.performed += inputActionEvent;
        inputAction.canceled += inputActionEvent;
    }

    public static void UnsubscribeToInputActionEvent(InputAction inputAction, Action<CallbackContext> inputActionEvent)
    {
        inputAction.started -= inputActionEvent;
        inputAction.performed -= inputActionEvent;
        inputAction.canceled -= inputActionEvent;
    }

    protected override void OnUpdate()
    {
        float inputH = moveData.x;
        float inputV = moveData.y;
        bool jump = jumpData;
        //Debug.Log("Jump state: " + jump);

        Entities.WithAll<RawInputData>().
            ForEach((ref RawInputData input, ref MoveData movement, ref RotateData rotateData, ref PhysicsData physicsData) => {
            //set input data
            input.inputH = inputH;
            input.inputV = inputV;
            input.jump = jump;

            //set direction data
            movement.targetDirection = new Unity.Mathematics.float3(input.inputH, 0, input.inputV);
            rotateData.rotateTargetPosition = movement.targetDirection;

                
        }).Schedule();
    }

    protected override void OnDestroy()
    {
        UnsubscribeToInputActionEvent(moveAction, OnMove);
        UnsubscribeToInputActionEvent(jumpAction, OnJump);
    }
}

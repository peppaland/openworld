using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

public class RotateTowards : SystemBase
{
    protected override void OnUpdate()
    {

        
        Entities.ForEach((ref Rotation rotation, in RotateData rotateData) => {

            //check if input not zero
            if (!rotateData.rotateTargetPosition.Equals(float3.zero))
            {
                //lerp current rotation to new input direction
                quaternion targetRotation = quaternion.LookRotationSafe(rotateData.rotateTargetPosition, math.up());
                rotation.Value = math.slerp(rotation.Value, targetRotation, rotateData.rotateSpeed);
            }


        }).Schedule();
    }
}

using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

public class LookAtTarget : SystemBase
{
    protected override void OnUpdate()
    {
        Entities.
            ForEach((ref RotateData rotateData, in Translation translation, in TargetData target) => {

            ComponentDataFromEntity<Translation> translationsArray = GetComponentDataFromEntity<Translation>(true);
            if (!translationsArray.HasComponent(target.lookAtEntity)) { return; }

            Translation targetPosition = translationsArray[target.lookAtEntity];
            rotateData.rotateTargetPosition = targetPosition.Value - translation.Value;

            if (!target.targetOffset.y.Equals(0f)) { return; }
            rotateData.rotateTargetPosition = new float3(rotateData.rotateTargetPosition.x, 0f, rotateData.rotateTargetPosition.z);


        }).Run();

    }
}

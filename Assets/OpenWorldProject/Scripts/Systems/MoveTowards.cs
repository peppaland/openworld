using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Transforms;
using UnityEngine;

public class MoveTowards : SystemBase
{
    
    protected override void OnUpdate()
    {
        float deltaTime = Time.DeltaTime;
        int moveH = Animator.StringToHash("moveH");
        int moveV = Animator.StringToHash("moveV");


        Entities.ForEach((ref PhysicsVelocity physVelocity, ref PhysicsMass physicsMass, in MoveData movement) => {
            physVelocity.Linear += movement.targetDirection * movement.moveSpeed * deltaTime;
            physicsMass.InverseInertia = float3.zero;

        }).Run();

        
        Entities.
            WithoutBurst().
            ForEach((Animator animator, in RawInputData input) => {
                animator.SetFloat(moveH, input.inputH);
                animator.SetFloat(moveV, input.inputV);
        }).Run();

    }
}

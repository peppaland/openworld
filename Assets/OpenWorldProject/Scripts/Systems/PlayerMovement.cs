using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public class PlayerMovement : SystemBase
{
    protected override void OnUpdate()
    {
        var cameraForward = Camera.main.transform.forward;
        var cameraRight = Camera.main.transform.right;
        float3 camRight = new float3(cameraRight.x, cameraRight.y, cameraRight.z);

        Entities.ForEach((ref Translation translation, ref MoveData movement, in RawInputData input) => {
            var playerForward = input.inputV;
            var playerRight = input.inputH;


            movement.targetDirection = playerForward * cameraForward;
            movement.targetDirection += playerRight * camRight;
            movement.targetDirection.y = 0f;

            //if(targetDirection.magnitude > 0)
            //{
                //player moves toward targetDirection
            //}
        }).Schedule();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

[UpdateAfter(typeof(PlayerTag))]
public class ConvertCamera : MonoBehaviour, IConvertGameObjectToEntity
{
    public EntityManager entityManager;

    public Entity targetEntity;
    public float3 offset;
    public float moveSpeed;
    public float rotateSpeed;
    

    private void Awake()
    {
        entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        
    }

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        dstManager.AddComponent<CopyTransformToGameObject>(entity);

        dstManager.AddComponentData(entity, new MoveData { moveSpeed = moveSpeed });
        dstManager.AddComponentData(entity, new TargetData { followEntity = targetEntity, lookAtEntity = targetEntity, targetOffset = offset });
        dstManager.AddComponentData(entity, new RotateData { rotateSpeed = rotateSpeed });
    }
}

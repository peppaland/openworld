using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;

[UpdateAfter(typeof(PlayerTag))]

public class ConvertPlayer : MonoBehaviour, IConvertGameObjectToEntity
{
    private Animator animator;
    public GameObject cameraObject;
    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        //add component data for transform of player to be driven by entities
        dstManager.AddComponent<CopyTransformToGameObject>(entity);

        //add reference to camera
        ConvertCamera convertCamera = cameraObject.GetComponent<ConvertCamera>();

        if(convertCamera == null)
        {
            convertCamera = cameraObject.AddComponent<ConvertCamera>();
        }

        convertCamera.targetEntity = entity;
    }

    private void Start()
    {
        animator = GetComponent<Animator>();
    }



}

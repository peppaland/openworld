using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using UnityEngine.InputSystem;

public class GameManager : MonoBehaviour, IConvertGameObjectToEntity
{

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        ProcessInputSystem inputSystem = World.DefaultGameObjectInjectionWorld.GetExistingSystem<ProcessInputSystem>();
        inputSystem.InitializePlayerInput(GetComponent<PlayerInput>());
    }

    private void Start()
    {
    }
}

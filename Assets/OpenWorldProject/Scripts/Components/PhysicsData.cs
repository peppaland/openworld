using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct PhysicsData : IComponentData
{

    public float jumpVelocity;
    public float jumpHeight;
    
}

using Unity.Entities;
using UnityEngine;

[GenerateAuthoringComponent]
public struct RawInputData : IComponentData
{
    [HideInInspector] public float inputH;    //horizontal input
    [HideInInspector] public float inputV;    //vertical input
    [HideInInspector] public bool jump;
    
}
